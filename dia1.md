# GIT

Sistema de control de versión

- Nos ayuda a controlar el código
- Nos ayuda a identificar diferencias... o a comprender como ha evolucionado el código
- Versiones: Fotos del código en un momento dado.
- Ayudar a sincronizar el trabajo de un equipo
-------------------------------------------------

Las versiones de nuestro código siguen una linea temporal?
RAMAS: Branches <<<<<

# Repositorio

Espacio donde subimos(controlo) el código de una aplicación
  |
  ---> Directorio (.git)
  
# Los 3 arboles de git:

- Directorio de trabajo (Working dir)

- Área de staging (Un espacio de almacenamiento intermedio que nos permite llevar cosas del WD -> Repo)

- Repositorio (BBDD con fotos y el historial de mi proyecto)

# COMMIT

Instantánea de mi proyecto

# Cambio?

Cambios en las lineas del contenido del archivos
Creación de un nuevo archivos
Eliminación de un archivo
Movimiento de un archivo


# HEAD

Variable que indica el commit en el que estoy posicionado:
- El commit a partir del cual se generará el siguiente commit
- El commit cuyos ficheros veo en el working dir

Por defecto, la variable HEAD, su valor es el del último commit que haya realizado
  en la rama en la que estoy ubicado.

HEAD    = Último commit
HEAD^   = Penúltimo commit
HEAD~1  = Penúltimo commit
HEAD^^  = Antepenúltimo commit
HEAD~2  = Antepenúltimo commit



REESCRIBIR EL HISTORIAL DE UN PROYECTO
    Historial: Secuencia de commits en REPO





vim
    i       Permite editar
    ESC     sale del modo edición
    :q!     Cierra sin guardar cambios
    :wq!    Cierra guardando cambios cambios
    
