3 zonas independientes:
- Directorio de trabajo local  ---  Working dir
- Área de staging
    Sirve para preparar los cambios que vamos a comitear
- Repositorio local                                     <<<<< .git   
    Guardamos un histórico de Commits.
    Un commit: Foto/instantanea de mi código
- Repositorios REMOTOS
----

init                            <<<< SOLO SE EJECUTA SI TRABAJAMOS EN UN PROYECTO NUEVO !!!
    config user.name
           user.email

CONTROLAR EL AREA DE STAGING
    
    add 
    reset 

GESTION DE LOS ARCHIVOS DE MI PROYECTO
    Borrar un archivo de GIT
        rm
            --cached
    Mover un archivo a otra carpeta
        mv

Consultar el historial de commits de mi proyecto
    log 
        --oneline

Diferencias: enre archivos, entre commits, para un archivo, entre lo que tengo en mi WD/Staging/REPO
    diff

commit -m "MENSAJE"
        -a     
        --amend        --no-edit
        
COMANDOS PARA CONTROLAR LOS COMMITS YA REALIZADOS EN GIT
    Deshacer un commit / Eliminarlo
        reset
            --soft      Borra el commit del repo
            --mixed     También el staging
            --hard      Además, restaura el WorkingDir
    Revertir un commit de forma no destructiva
        revert
    Me permite ver un archivo (ponerlo en el WorkingDir) en una determinada versión (commit)
        Restaurar un archivo a una determina versión
        
        checkout   <<<<   Cambiar el HEAD a un determinado COMMIT
        
    cherry-pick
        Aplicar los cambios que se incorporaron en un commit
    
    rebase


reset
    1º Se mueve el HEAD
    2º Se desvinculan los commits posteriores

HEAD?
    Una Variable que indicaba el commit en el que estaba posicionado en un momento dado.
    Por defecto su valor es último commit que haya realizado.
    
    Qué significa estar posicionado en un commit?
        Que cuando haga el próximo COMMIT se realizará vinculado temporalmente(TIEMPO) al que estoy 

    
    C1 >  C2  > C3*  >  C4  >  C5


            ES UNA RUTA
                V
git checkout C3 .                       Mis archivos en mi WD se restaurarían a cómo estaban en el C3
                                        Donde quedaría en HEAD? C5
                                        
git checkout C3

git checkout RAMA
    Poner el HEAD en el último COMMIT de la rama

MOVER EL HEAD, implica: restaurar mi WD a cómo estaba en un COMMIT
* En mi Working dir, veo siempre los archivos tal y como están en el HEAD


----- RAMAS -----------------------------------------------------------------------

RAMA? Otra linea temporal de mi proyecto

Para qué sirven?
    Controlar evoluciones en paralelo de mi proyecto
        Para qué querría hacer eso?
            Aplicación > V1 >>>> Mantener la V1
                            >>>> Evolucionar el proeycto V2
            Si desarrollo una funcionalidad que aún no se va a incorporar al proyecto final
            
Libreria 
    V1: 
        Suma
        Resta ** BUG
    
    V2:
        Suma
        Multiplicacion
        División
            
                    Producción
                    VVV 
    Alta > Suma > Resta > Multiplicación  > Divis*

git checkout RESTA
                    Producción
                    VVV 
    Alta > Suma > Resta* > Multiplicación  > Divis

PERO: No puedo hacer nuevos commits

git reset RESTA
                    Producción
                    VVV 
    Alta > Suma > Resta*

GUAY: YA PUEDO HACER NUEVOS COMMITS...
PERO? HE perdido lo nuevo


            
                    Producción
                    VVV 
    Alta > Suma > Resta > Multiplicación  > División <<<< Producción
                        
                        > FIX_RESTA * <<<< Producción / MUERE

RAMA PRINCIPAL: master / main
    DEFINICIÓN DEL CONTENIDO DE LA RAMA: En esta rama está lo que sube a producción
    REGLA DE ORO: Nunca JAMAS VOY A REALIZAR UN COMMIT
    
hot-fix                               ---C11--                                    
                                     /        \
master --- C1 -----------C3-------- C5 --------C11----------------C14----------------------------
             \          /          /                             /
desarrollo    --C2----C3---C4----C5---C6-----C7-- C12(C11) -- C14-------------------------------------------
                      \      \                          /      /
caracteristica1        \      --C8---C9----C10-------C13------/---------------------------------------------------
                        \                                    /
sass                     \------CA  ----- CB ----- CC   ---CD 

Cómo llamamos a la operación de juntar el código de 2 ramas?  MERGE (FUSION DE CAMBIOS)

Falso merge... falsas fusiones: Fast-forward





git branch                > Listado de ramas actual, indicando la rama en la que estoy posicionado
*git branch NOMBRE_RAMA      < Crear una rama
git branch -d NOMBRE_RAMA   < Borrar una rama
git branch -m NOMBRE_RAMA NOMBRE_RAMA_2  < Renombrar una rama
*git checkout RAMA           < Cambiar a una rama
        git checkout -b RAMA   <<< Crear una rama y posicionarme en ella   ***
git merge           <<<< Mantiene la historia real del proyecto
git rebase          <<<< Cambia el historial de proyecto
                            Más facil de entender si la cuento de otra forma
                            
                            
                            
master --- Alta----------------------------------Resta--------------------------------Op Mult y vabs
            \                                   /                                    /
desarrollo   --- Comienzo ----- Suma ----- Resta --- Valor absoluto------ Op Mult y vabs
                                                \                       /               \    
multiplicacion                                   Resta-----Multiplicar------------------- Op Mult y vabs




                            
master --- Alta--------------------------------------------------------------------Multiplicar
            \                                                                          /
desarrollo   --- Comienzo ----- Suma ----- Resta --- Valor absoluto                   /
                                                                \                    /   
multiplicacion                                                   ----Multiplicar----/




Master:
 Alta   >   Comienzo  > Suma   > Resta  > Valor Absoluto  > Multiplicar  



master, desarrollo :  Alta  --- Comienzo ----- Suma ----- Resta 




master ----V1-------------V1.1-------------------V2---------                                css acabado
                                                  \                                       V
desarrollo                                         ----------C1---------C4-------------- C6
                                                                         \              /
maricarmen                                                                --C2---C3--C5
                                                                                     ^ Pues parece que ya está
                                                                                 ^ Que ganas de un cafecitoi
                                                                            ^ Buffff. me duele la cabeza y me piro a dar una vuelta

desarrollo
    V1 > V1.1 > V2 > C1 > C4 > C6
    
    
    git merge --squash
    
    
------------ REPO REMOTO ----------------------------------------------------
Un repo de un tipo bare (no como el que tengo en local) que existe en un servidor accesible por red

REPO BARE:
    Un repo que solo tiene REPO... no tiene ni:
        - Area de staging 
        - Directorio de trabajo
    Se usan para sincronizar commits entre repos locales
    
    
                    desarrollo F1, SCSS
                    REPO REMOTO: Bitbucket
                        |
    ------------------------------------
    |                                   |
 REPO.local                         REPO.local
    ^                                   ^
 area staging                       area staging
    ^                                   ^
 directorio de trabajo          directorio de trabajo
 ---------------------          ----------------------
         PC                              PC
        Carlos                        Maricarmen
        
    master                              master
    desarrollo F1 , SCSS                desarrollo F1   SCSS
        ^                                 V             ^
      carlos F1                         maricarmen   CSS Precioso!!!
      
        V
    REPO REMOTO DE CARLOS
        Github: Cuenta privada

La sincronización se realiza a nivel de RAMA



REMOTO INTERNO HISTORIAL
    ^^
desarrollo IBERMATICA
    VV
REPO REMOTO CLIENTE Solo le publico la rama MASTER
    >>>> Cliente instalacion


Un remoto va a tener un IDENTIFICADOR y además URL


REPO LOCAL   >>>>>   REPO REMOTO      [git push]
REPO REMOTO  >>>>>   REPO LOCAL       [git fetch]
                                          git merge
                                        
                                            git pull = git fetch + git merge
C1----C2-----C4                                        
              V                          
C1----C2-----C3* > C5




