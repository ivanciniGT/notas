git init

git config --global user.name "Ivan"
git config --global user.email "ivan.osuna.ayuste@gmail.com"

git status

git add <RUTAS>
    git add README.md
    git add README.md otro_archivo.txt
    git add * # No añade archivos ocultos
    git add .
    git add :/ # Añade todo desde el root del proyecto

git reset <RUTAS>
    # git reset --mixed HEAD <RUTAS>

git commit -m "MENSAJE DE CONFIRMACION"

git commit -am "Operación suma"

git diff <RUTA>
git diff --cached <RUTA>
    # Diferencias entre staging y el ultimo commit (HEAD)
git diff HEAD <RUTA>

git diff HEAD HEAD^ <RUTA>

git log --oneline

git mv libreria.yaml src

git rm <RUTA>
    Borra un archivo de la carpeta mia (WD)
    Apuntar en staging que se ha borrado un archivo

git rm --cached <RUTA>
    Apuntar en staging que se ha borrado un archivo

git checkout <RUTA>
    git checkout HEAD <RUTA>


# Reescritura del historial
Queremos un historial de proyecto LIMPIO

git commit --amend -m ''
                   --no-edit

Cuando?
    - Me he equivocado en algo
    - Me faltaba algo, por olvido
    *** Me faltaba algo... porque no lo había hecho intencionalmente
        - Lo commiteo porque me voy a tomar un cafe
        - Lo commiteo porque ya me piro al gimnasio
        - Lo commiteo porque me voy a echar la siesta
        
git reset COMMIT_ID

    Cambiar el HEAD al ID que le suministre
    Desvincular todos los commit posteriores al nuevo HEAD
    
    --soft                  
    --mixed (POR DEFECTO)   Borra el área de staging adicionalmente
    --hard *** PELIGROSO    Además, cambia el WD a la instantanea del HEAD nuevo

git reset 4a66684
    git reset HEAD^
    
    
git gc  ----- GARBAGE COLLECTOR
    
git revert ID_COMMIT 
    A nivel del repo, se generará un NUEVO COMMIT 
    que anule el que estoy suministrando como argumento
    

git reset --hard HEAD^
git revert HEAD


git checkout ID_COMMIT FICHERO
        
        
git cherry-pick ID_COMMIT
